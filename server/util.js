/* jshint esversion: 6, asi: true, node: true */
// util.js

// private
const debug = require('debug')('WebSSH2');
const Auth = require('basic-auth');
const jwt = require('jsonwebtoken');
const crypto = require('crypto')

let defaultCredentials = { username: null, password: null, privatekey: null };

exports.setDefaultCredentials = function setDefaultCredentials({
  name: username,
  password,
  privatekey,
  overridebasic,
}) {
  defaultCredentials = { username, password, privatekey, overridebasic };
};

exports.basicAuth = function basicAuth(req, res, next) {
  const myAuth = Auth(req);
  // If Authorize: Basic header exists and the password isn't blank
  // AND config.user.overridebasic is false, extract basic credentials
  // from client]
  const { username, password, privatekey, overridebasic } = defaultCredentials;
  if (myAuth && myAuth.pass !== '' && !overridebasic) {
    req.session.username = myAuth.name;
    req.session.userpassword = myAuth.pass;
    debug(`myAuth.name: ${myAuth.name} and password ${myAuth.pass ? 'exists' : 'is blank'}`);
  } else {
    req.session.username = username;
    req.session.userpassword = password;
    req.session.privatekey = privatekey;
  }
  if (!req.session.userpassword && !req.session.privatekey) {
    res.statusCode = 401;
    debug('basicAuth credential request (401)');
    res.setHeader('WWW-Authenticate', 'Basic realm="WebSSH"');
    res.end('Username and password required for web SSH service.');
    return;
  }
  next();
};

// takes a string, makes it boolean (true if the string is true, false otherwise)
exports.parseBool = function parseBool(str) {
  return str.toLowerCase() === 'true';
};

function generateJWT(data, secret=process.env.SECRET_KEY) {
  return jwt.sign(data, secret, { expiresIn: '1 hour' })
};

function verifyJWT(token, secret=process.env.SECRET_KEY) {
  if (!token) throw 'EMPTY_TOKEN';
  const user = jwt.verify(token, secret)
  if (!user) throw 'INVALID_TOKEN';
  return user;
}


exports.encrypt = function(text) {
  if ( typeof text=="object" ) text = JSON.stringify(text);
  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv('aes-256-ctr', process.env.SECRET_KEY, iv);
  const encrypted = Buffer.concat([cipher.update(text), cipher.final()]);
  return generateJWT({
    iv: iv.toString('hex'),
    content: encrypted.toString('hex')
  });
}
exports.decrypt = function(signed) {
  try {
    const hash = verifyJWT(signed);
    const decipher = crypto.createDecipheriv(
      'aes-256-ctr', 
      process.env.SECRET_KEY, 
      Buffer.from(hash.iv, 'hex')
    );
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
    return JSON.parse(decrpyted.toString());
  } catch(e) {
    console.log(e);
    return {}
  }
}
